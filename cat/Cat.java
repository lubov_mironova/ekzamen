package cat;

public class Cat {
    private String name;
    private String birthYear;
    private String breed;
    private Gender gender;

    public Cat(String name, String birthYear, String breed, Gender gender) {
        this.name = name;
        this.birthYear = birthYear;
        this.breed = breed;
        this.gender = gender;
    }

    public Cat(String name, String birthYear, String breed) {
        this.name = name;
        this.birthYear = birthYear;
        this.breed = breed;
    }

    public boolean isMale(){
        return this.gender == Gender.MALE;
    }

    public int year(){
        String[] catYear = this.birthYear.split("\\.");
        int catBirthYear = Integer.valueOf(catYear[2]);
        return catBirthYear;
    }

    public String getBirthYear() {
        return birthYear;
    }

    @Override
    public String toString() {
        return "Cat{" +
                "Кличка='" + name + '\'' +
                ", пол=" + gender +
                ", дата рождения='" + birthYear + '\'' +
                ", порода='" + breed + '\'' +
                '}';
    }
}
