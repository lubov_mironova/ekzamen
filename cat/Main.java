package cat;

public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Муся", "02.05.2004", "сиамская", Gender.FEMALE);
        Cat cat2 = new Cat("Буся", "02.05.2007", "сибирская", Gender.FEMALE);
        Cat cat3 = new Cat("Снежок", "02.05.2003", "сфинкс", Gender.MALE);
        Cat cat4 = new Cat("Барсик", "02.05.2007", "британская", Gender.MALE);
        Cat cat5 = new Cat("Том", "02.05.2003", "мейн-кун", Gender.MALE);

        Cat[] cats = {cat1, cat2, cat3, cat4, cat5};

        System.out.println("Самый старый кот:");
        oldMaleCat(cats);
    }

    public static int oldestCat(Cat[] cats){
        int min = cats[0].year();
        for (int i = 0; i < cats.length; i++){
            if (cats[i].year() < min && cats[i].isMale()) {
                min = cats[i].year();
            }
        } return min;
    }

    public static void oldMaleCat(Cat[] cats){
        int min = oldestCat(cats);
        for (int i = 0; i < cats.length; i++) {
            if(min == cats[i].year()){
                System.out.println(cats[i] + "");
            }
        }
    }
}
